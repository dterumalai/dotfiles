# Shortcuts
alias copyssh="pbcopy < $HOME/.ssh/id_ed25519.pub"
alias reloadshell="omz reload"
alias reloaddns="dscacheutil -flushcache && sudo killall -HUP mDNSResponder"
alias ll="/opt/homebrew/opt/coreutils/libexec/gnubin/ls -AhlFo --color --group-directories-first"
alias phpstorm='open -a PhpStorm.app "`pwd`"'
alias sublime="subl"

# Laravel
alias sail="./vendor/bin/sail"
alias artisan="./vendor/bin/sail art"
alias fresh="./vendor/bin/sail art migrate:fresh --seed"
alias seed="./vendor/bin/sail art db:seed"
alias routes="./vendor/bin/sail art route:list"
alias start="./vendor/bin/sail up -d"
alias stop="./vendor/bin/sail down"
